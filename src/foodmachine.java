import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class foodmachine {
    private JPanel root;
    private JLabel toplabel;
    private JTextPane receivedinfo;
    private JButton checkOutButton;
    private JLabel toplabel2;
    private JLabel totalinfo;
    private JButton Tempurabutton;
    private JButton udonButton;
    private JButton Karaagebutton;
    private JButton gyouzaButton;
    private JButton ramenButton;
    private JButton yakisobaButton;
    int total = 0;
    int price;

    public static void main(String[] args) {
        JFrame frame = new JFrame("foodmachine");
        frame.setContentPane(new foodmachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order(String food) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?", "Order comfirmation", JOptionPane.YES_NO_OPTION);

        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering " + food + "! It will be served as soon as possible.");
            receivedinfo.setText(receivedinfo.getText()+food+"\n");
            total +=price;
            totalinfo.setText("Total " + total + "yen");
        }
    }

    public foodmachine() {
        Tempurabutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price = 100;
                order("Tempura");
            }
        });
        Karaagebutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price = 100;
                order("Karaage");
            }
        });
        gyouzaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price = 100;
                order("Gyouza");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price = 100;
                order("Udon");
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price = 100;
                order("Yakisoba");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price = 100;
                order("Ramen");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?", "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null,
                            "Thank you.The total price is " + total + " yen");
                    total = 0;
                    totalinfo.setText("Total "+total+"yen");
                    receivedinfo.setText("");
                }
            }
        });

    }
}
